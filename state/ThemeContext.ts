import { createContext } from "react";
import { ThemeContextType } from "../types";

const ThemeContext = createContext<ThemeContextType>(null);

export default ThemeContext;
