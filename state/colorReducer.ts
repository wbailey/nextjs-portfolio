import { ColorStateType, ColorActionType } from "@/types";

export default function colorReducer(
	state: ColorStateType,
	action: ColorActionType
): ColorStateType {
	return { ...state, [action.name]: action.color };
}
