import { ThemeActionsType, ThemeContextType } from "../types";
import { Themes } from "../utils";

export default function themeReducer(
	_state: ThemeContextType,
	action: ThemeActionsType
): ThemeContextType {
	const theme: ThemeContextType = Themes.getTheme(action);
	return theme;
}
