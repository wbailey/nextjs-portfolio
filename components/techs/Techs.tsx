import { useId } from "@/hooks";
import { ThemeContext } from "@/state";
import { useContext } from "react";
import { techs } from "@/copy";
import { ScrollRefType, ThemeContextType } from "@/types";
import { TechPill, TechPillContainer, TechSection } from "./styled";

interface Props {
	scrollRef: ScrollRefType;
}

export default function Techs({ scrollRef }: Props) {
	const theme: ThemeContextType = useContext(ThemeContext);

	const componentId = useId("tech-section");

	const techPills = techs.map((tech, i) => {
		return (
			<TechPill theme={theme} key={`${componentId}-techpill${i}`}>
				<p>{tech}</p>
			</TechPill>
		);
	});

	return (
		<div ref={scrollRef}>
			<TechSection theme={theme}>
				<h2>Techs</h2>

				<TechPillContainer>{techPills}</TechPillContainer>
			</TechSection>
		</div>
	);
}
