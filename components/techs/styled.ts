import styled from "styled-components";
import { fontSizes, units, layout } from "@/components/common/styled/css";
import { ThemeProps } from "@/types";

export const TechPill = styled.div`
	${fontSizes.small}
	background-color: ${(props: ThemeProps) => props.theme.colors.tertiary};
	margin: 1vw;
	padding: 0.5vw;
	/* border-radius: 10px; */
	& p {
		margin: 0;
		color: ${(props: ThemeProps) => props.theme.colors.primary};
	}
`;

export const TechPillContainer = styled.div`
	${fontSizes.small}
	${layout.rowStatic}
	flex-wrap: wrap;
`;

export const TechSection = styled.section`
	padding: calc(${units.font.small} * 3) 0;
	min-height: 50vh;
	background-color: ${(props: ThemeProps) => props.theme.colors.secondary};
	& h2 {
		${fontSizes.med}
		padding-left: 2vw;
		color: ${(props: ThemeProps) => props.theme.colors.primary};
	}
`;
