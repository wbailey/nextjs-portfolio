export { default as About } from "./about/About";
export { default as Landing } from "./landing/Landing";
export { default as Projects } from "./projects/Projects";
export { default as Techs } from "./techs/Techs";
