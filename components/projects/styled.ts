import styled from "styled-components";
import { fontSizes, units, layout } from "@/components/common/styled/css";
import { ThemeProps } from "@/types";

export const Card = styled.div`
	padding: ${units.font.med};
	margin: calc(${units.font.tiny} * 0.5) 2vw;
	border: calc(${units.font.tiny} * 0.5) solid
		${(props: ThemeProps) => props.theme.colors.secondary};
	${layout.row}
	@media screen and (max-width: ${units.breakpoint.tablet}) {
		margin: calc(${units.font.tiny} * 0.5);
		align-items: center;
		justify-content: center;
		text-align: center;
	}
`;

export const Img = styled.img`
	max-width: 400px;
	@media screen and (max-width: ${units.breakpoint.tablet}) {
		max-width: 350px;
	}
`;

export const TextSection = styled.div`
	padding: 0 ${units.font.med};
	width: 100%;
	${layout.col}
	& h3 {
		color: ${(props: ThemeProps) => props.theme.colors.primary};
		background-color: ${(props: ThemeProps) =>
			props.theme.colors.secondary};
		padding: 0.5vw;
		/* border-radius: 10px; */
		${fontSizes.small}
	}
	& p {
		color: ${(props: ThemeProps) => props.theme.colors.primary};
		background-color: ${(props: ThemeProps) => props.theme.colors.tertiary};
		padding: 0.5vw;
		/* border-radius: 10px; */
		${fontSizes.xSmall}
	}
`;

export const Section = styled.section`
	padding: calc(${units.font.small} * 3) 0;
	min-height: 100vh;
	background-color: ${(props: ThemeProps) => props.theme.colors.primary};
`;

export const SectionHeader = styled.h2`
	padding-left: 2vw;
	color: ${(props: ThemeProps) => props.theme.colors.tertiary};
	${fontSizes.med}
`;

export const ProjectsContainer = styled.div`
	${layout.col}
`;

export const SmallProjectsHeader = styled.h3`
	padding-left: 2vw;
	color: ${(props: ThemeProps) => props.theme.colors.tertiary};
	${fontSizes.small}
`;

export const SmallProjectListItem = styled.li`
	color: ${(props: ThemeProps) => props.theme.colors.tertiary};
	${fontSizes.tiny}
`;
