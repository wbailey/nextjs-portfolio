import { Anchor } from "@/components/common/styled";
import { ThemeContext } from "@/state";
import { useContext } from "react";
import { Img, TextSection, Card } from "./styled";

interface Props {
	img: {
		src: string;
		alt: string;
	};
	title: string;
	description: string;
	link: string;
}

export default function Project({ img, title, description, link }: Props) {
	const theme = useContext(ThemeContext);

	return (
		<Card theme={theme}>
			<Img src={img.src} alt={img.alt} />
			<TextSection theme={theme}>
				<h3>{title}</h3>

				<p>{description}</p>

				<Anchor href={link} target="_blank" theme={theme}>
					{link.includes("github")
						? `Visit ${title} on Github`
						: link.includes("gitlab")
							? `Visit ${title} on Gitlab`
							: `Visit ${title}`}
				</Anchor>
			</TextSection>
		</Card>
	);
}
