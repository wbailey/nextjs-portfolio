import { useContext } from "react";
import { ThemeContext } from "@/state";
import Project from "./Project";
import { Anchor } from "@/components/common/styled";
import { projects, smallProjects } from "@/copy";
import { ScrollRefType } from "@/types";
import {
	Section,
	SectionHeader,
	ProjectsContainer,
	SmallProjectsHeader,
	SmallProjectListItem,
} from "./styled";

interface Props {
	scrollRef: ScrollRefType;
}

export default function Projects({ scrollRef }: Props) {
	const theme = useContext(ThemeContext);

	const projectCards = projects.map((project, i) => {
		return (
			<Project
				key={`project-${i}`}
				img={project.img}
				title={project.title}
				description={project.description}
				link={project.link}
			/>
		);
	});

	const smallProjectsList = smallProjects.map((project, i) => {
		return (
			<SmallProjectListItem theme={theme} key={`smolboi${i}`}>
				<>
					<Anchor href={project.link} target="_blank" theme={theme}>
						{project.title}
					</Anchor>
					&nbsp;
					{project.text}
				</>
			</SmallProjectListItem>
		);
	});
	return (
		<div ref={scrollRef}>
			<Section theme={theme}>
				<SectionHeader theme={theme}>Projects</SectionHeader>

				<ProjectsContainer>{projectCards}</ProjectsContainer>

				<SmallProjectsHeader theme={theme}>
					Smaller, but still notable projects
				</SmallProjectsHeader>

				<ul>{smallProjectsList}</ul>
			</Section>
		</div>
	);
}
