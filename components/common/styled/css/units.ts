export default {
	font: {
		large: "calc(0.095 * 100vw)",
		medLarge: "calc(0.065 * 100vw)",
		med: "calc(0.03 * 100vw)",
		small: "calc(0.017 * 100vw)",
		xSmall: "calc(100vw * 0.015)",
		tiny: "calc(100vw * 0.012)",
		mobile: {
			large: "calc(0.2 * 100vw)",
			medLarge: "calc(0.065 * 100vw)",
			med: "calc(0.07 * 100vw)",
			small: "calc(0.04 * 100vw)",
			xSmall: "calc(100vw * 0.04)",
			tiny: "calc(100vw * 0.04)",
		},
	},
	breakpoint: {
		tablet: "1100px",
	},
};
