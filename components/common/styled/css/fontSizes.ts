import { css } from "styled-components";
import units from "./units";

const large = css`
	font-size: ${units.font.large};
	@media screen and (max-width: ${units.breakpoint.tablet}) {
		font-size: ${units.font.mobile.large};
	}
`;

const medLarge = css`
	font-size: ${units.font.medLarge};
	@media screen and (max-width: ${units.breakpoint.tablet}) {
		/* font-size: ${units.font.mobile.medLarge}; */
		font-size: inherit;
	}
`;
const med = css`
	font-size: ${units.font.med};
	@media screen and (max-width: ${units.breakpoint.tablet}) {
		font-size: ${units.font.mobile.med};
	}
`;

const small = css`
	font-size: ${units.font.small};
	@media screen and (max-width: ${units.breakpoint.tablet}) {
		font-size: ${units.font.mobile.small};
	}
`;

const xSmall = css`
	font-size: ${units.font.xSmall};
	@media screen and (max-width: ${units.breakpoint.tablet}) {
		font-size: ${units.font.mobile.xSmall};
	}
`;

const tiny = css`
	font-size: ${units.font.tiny};
	@media screen and (max-width: ${units.breakpoint.tablet}) {
		font-size: ${units.font.mobile.tiny};
	}
`;

export default {
	large,
	med,
	small,
	xSmall,
	tiny,
};
