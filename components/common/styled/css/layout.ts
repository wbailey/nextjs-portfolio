import { css } from "styled-components";
import units from "./units";

const row = css`
	display: flex;
	flex-direction: row;
	@media screen and (max-width: ${units.breakpoint.tablet}) {
		flex-direction: column;
	}
 }
`;

const rowStatic = css`
	display: flex;
	flex-direction: row;
`;

const col = css`
	display: flex;
	flex-direction: column;
`;
const hideMobile = css`
	@media screen and (max-width: ${units.breakpoint.tablet}) {
		display: none;
	}
`;

export default {
	row,
	rowStatic,
	col,
	hideMobile,
};
