import styled from "styled-components";
import { units, layout, fontSizes } from "@/components/common/styled/css";
import { ThemeProps } from "@/types";

export const Anchor = styled.a`
	color: ${(props: ThemeProps) => props.theme.colors.secondary};
	background-color: ${(props: ThemeProps) => props.theme.colors.primary};
	text-decoration: none;
	padding: 0 0.5vw;
	${fontSizes.tiny}
	:hover {
		text-decoration: underline;
		color: ${(props: ThemeProps) => props.theme.colors.tertiary};
	}
`;
