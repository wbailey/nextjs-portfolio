import styled from "styled-components";
import { fontSizes, units, layout } from "@/components/common/styled/css";
import { ThemeProps } from "@/types";

export const IconImg = styled.img`
	padding: 0 ${units.font.xSmall};
	width: ${units.font.med};
	height: ${units.font.med};
	/* calculated using getSvgColors hook */
	${(props: { color: string }) => props.color}
	@media screen and (max-width: ${units.breakpoint.tablet}) {
		padding: 0 ${units.font.mobile.xSmall};
		width: ${units.font.mobile.med};
		height: ${units.font.mobile.med};
	}
`;

export const IconLink = styled.a`
	${layout.col}
	color: inherit; /* needed? */
	text-decoration: none;
	top: 0;
	margin: 0;
	justify-content: center;
	align-items: center;
	& > p {
		${fontSizes.tiny}
		color: ${(props: ThemeProps) => props.theme.colors.tertiary};
	}
`;
