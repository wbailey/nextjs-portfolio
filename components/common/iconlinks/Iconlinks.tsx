import { useState, useContext } from "react";
import { useId } from "@/hooks";
import { ThemeContext } from "@/state";
import { DataObjectArray, DataObjectType, ThemeContextType } from "@/types";
import { IconLink, IconImg } from "./styled";

interface Props {
	iconLinks: Array<{
		id: string;
		src: string;
		alt: string;
		href: string;
	}>;
}

export default function IconLinks(props: Props) {
	const [mouse, setMouse] = useState<string>("");

	const theme: ThemeContextType = useContext(ThemeContext);

	const componentId: string = useId("IconLinks");

	const iconLinkComponents = props.iconLinks.map((icon, index) => {
		const filter =
			icon.id === mouse ? theme.svg.active : theme.svg.inActive;
		return (
			<IconLink
				key={`IconLink-${index}-${componentId}`}
				target="_blank"
				href={icon.href}
				onMouseEnter={() => setMouse(icon.id)}
				onMouseLeave={() => setMouse("")}
				theme={theme}
			>
				<IconImg src={icon.src} alt={icon.alt} color={filter} />

				<p>{icon.id}</p>
			</IconLink>
		);
	});

	return <>{iconLinkComponents}</>;
}
