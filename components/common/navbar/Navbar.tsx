import {
	useState,
	useEffect,
	useContext,
	useReducer,
	MouseEvent,
	ChangeEvent,
} from "react";
import {
	NavListItemHover,
	NavListItemActive,
	NavListItemInactive,
	NavUnorderedList,
	Label,
	SelectContainer,
	ThemeControls,
	LinkContainer,
} from "./styled";
import { useId } from "@/hooks";
import { ThemeContext, colorReducer } from "@/state";
import { Themes } from "@/utils";
import {
	ScrollRefsType,
	ThemeContextType,
	ThemeActionsType,
	ColorNameType,
} from "@/types";

const navItems = ["contact", "about", "techs", "projects"];

interface Props {
	scrollRefs: ScrollRefsType;
}

export default function NavBar({ scrollRefs }: Props) {
	const [select, setSelected] = useState<string>("contact");
	const [hover, setHover] = useState<string>("");
	const [color, dispatchColor] = useReducer(colorReducer, {
		primary: "",
		secondary: "",
		tertiary: "",
	});
	// const [hamburger, setHamburger] = useState<string>("hamburger-closed.svg");

	const theme: ThemeContextType = useContext(ThemeContext);

	const componentId: string = useId("Nav");

	useEffect(() => {
		if (scrollRefs[select].current) {
			scrollRefs[select].current?.scrollIntoView();
		}
	}, [select]);

	const handleNavClick = (e: MouseEvent<HTMLElement>) =>
		setSelected((e.target as HTMLElement).innerText);

	const handleHover = (e: MouseEvent<HTMLElement>) =>
		setHover((e.target as HTMLElement).innerText);

	// const handleHamburgerClick = () => {
	// 	hamburger === "hamburger-closed.svg"
	// 		? setHamburger("hamburger-open.svg")
	// 		: setHamburger("hamburger-closed.svg");
	// };

	const handleSelectChange = (e: ChangeEvent<HTMLSelectElement>) => {
		if (theme?.dispatchTheme) {
			const newTheme = (e.target as HTMLSelectElement)
				.value as ThemeActionsType;
			if (
				newTheme === "custom" &&
				!Themes.checkThemeIsRegistered("custom")
			) {
				const newCustomTheme = new Themes(
					"custom",
					theme.colors.primary,
					theme.colors.secondary,
					theme.colors.tertiary
				);
				dispatchColor({ name: "primary", color: theme.colors.primary });
				dispatchColor({
					name: "secondary",
					color: theme.colors.secondary,
				});
				dispatchColor({
					name: "tertiary",
					color: theme.colors.tertiary,
				});
				newCustomTheme.calculateSvgColors();
				newCustomTheme.register();
			}
			theme.dispatchTheme(newTheme);
		}
	};

	const colorChangeHandler = (e: any): void => {
		const name = (e.target as HTMLInputElement).name as ColorNameType;
		const color = (e.target as HTMLInputElement).value;
		// we need to re-render the whole app
		theme.forceRender(() => {
			const customTheme = Themes.getTheme("custom");
			customTheme.colors[name] = color;
			customTheme.calculateSvgColors();
			dispatchColor({ name, color });
		});
	};

	const navItemComponents = navItems.map((navItem, index) => (
		<a
			key={`a-${componentId}-${index}}`}
			href={`#${navItem}`}
			onClick={handleNavClick}
		>
			{select === navItem ? (
				<NavListItemActive
					onMouseEnter={handleHover}
					onMouseLeave={() => setHover("")}
					theme={theme}
				>
					{navItem}
				</NavListItemActive>
			) : hover === navItem ? (
				<NavListItemHover
					onMouseEnter={handleHover}
					onMouseLeave={() => setHover("")}
					theme={theme}
				>
					{navItem}
				</NavListItemHover>
			) : (
				<NavListItemInactive
					onMouseEnter={handleHover}
					onMouseLeave={() => setHover("")}
					theme={theme}
				>
					{navItem}
				</NavListItemInactive>
			)}
		</a>
	));

	const colorInputs: JSX.Element = (
		<>
			<Label htmlFor="primary">Primary Color:</Label>
			<input
				id="primary"
				type="color"
				value={color.primary}
				name={"primary"}
				onChange={colorChangeHandler}
			/>

			<Label htmlFor="secondary">Secondary Color:</Label>
			<input
				id="secondary"
				type="color"
				value={color.secondary}
				name={"secondary"}
				onChange={colorChangeHandler}
			/>

			<Label htmlFor="tertiary">Tertiary Color:</Label>
			<input
				id="tertiary"
				type="color"
				value={color.tertiary}
				name={"tertiary"}
				onChange={colorChangeHandler}
			/>
		</>
	);

	return (
		<div>
			<NavUnorderedList theme={theme}>
				<ThemeControls theme={theme}>
					<SelectContainer>
						<Label htmlFor="theme-select">Theme:</Label>

						<select
							id="theme-select"
							value={theme?.name}
							onChange={handleSelectChange}
						>
							<option value="dark">Dark</option>
							<option value="light">Light</option>
							<option value="custom">Custom</option>
							<option disabled value="geocities">
								Geo Cities
							</option>
						</select>

						{theme.name === "custom" && colorInputs}
					</SelectContainer>
				</ThemeControls>

				<LinkContainer>{navItemComponents}</LinkContainer>
			</NavUnorderedList>
		</div>
	);
}
