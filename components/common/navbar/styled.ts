import styled from "styled-components";
import { fontSizes, units, layout } from "@/components/common/styled/css";
import { ThemeProps } from "@/types";

export const NavListItemHover = styled.li`
	color: ${(props: ThemeProps) => props.theme.colors.primary};
	background-color: ${(props: ThemeProps) => props.theme.colors.tertiary};
`;

export const NavListItemActive = styled.li`
	color: ${(props: ThemeProps) => props.theme.colors.primary};
	background-color: ${(props: ThemeProps) => props.theme.colors.secondary};
`;

export const NavListItemInactive = styled.li`
	color: ${(props: ThemeProps) => props.theme.colors.tertiary};
	background-color: ${(props: ThemeProps) => props.theme.colors.primary};
`;

export const Label = styled.label`
	display: inline-block;
	margin: 0 ${units.font.xSmall} 0 ${units.font.tiny};
`;

export const SelectContainer = styled.div`
	display: flex;
	align-items: center;
	justify-content: center;
	& select {
		margin: ${units.font.tiny} 0;
		${fontSizes.tiny}
	}
	& label {
		@media screen and (max-width: ${units.breakpoint.tablet}) {
			display: none;
		}
	}
	& input {
		@media screen and (max-width: ${units.breakpoint.tablet}) {
			margin: 0 ${units.font.mobile.tiny};
		}
	}
`;

// https://dev.to/stephencweiss/reusing-css-with-styled-components-3okn
export const NavUnorderedList = styled.ul`
	/* from .navBarPosition */
	z-index: 1000;
	width: 100%;
	padding: 0; 
	margin: 0;
	position: fixed;
	top: 0;
	width: 100%;
	/* from .nav, .row, .justifyRight */
	list-style-type: none;
	${fontSizes.xSmall}
	background-color: ${(props: ThemeProps) => props.theme.colors.primary};
	& li {
		padding: ${units.font.small} ${units.font.xSmall};
	}
	& a,
	a:hover,
	a:focus,
	a:active {
		color: inherit;
		text-decoration: none;
	}
	${layout.row}
	justify-content: right;
}
`;

export const ThemeControls = styled.div`
	width: 100%;
	align-items: center;
	justify-content: start;
	color: ${(props: ThemeProps) => props.theme.colors.secondary};
	${layout.row}
`;

export const LinkContainer = styled.div`
	${layout.row};
	${layout.hideMobile}
`;
