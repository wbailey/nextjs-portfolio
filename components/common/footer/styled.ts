import styled from "styled-components";
import { layout } from "@/components/common/styled/css";
import { ThemeProps } from "@/types";

export const FooterContainer = styled.footer`
	align-items: center;
	justify-content: center;
	background-color: ${(props: ThemeProps) => props.theme.colors.primary};
	color: ${(props: ThemeProps) => props.theme.colors.tertiary};
	${layout.col}
`;

export const FooterIconContainer = styled.div`
	align-items: center;
	justify-content: center;
	${layout.rowStatic}
`;
