import { useContext } from "react";
import { ThemeContext } from "@/state";
import { ThemeContextType } from "@/types";
import { iconLinks } from "@/copy";
import { IconLinks } from "@/components/common";
import { Anchor } from "@/components/common/styled";
import { FooterContainer, FooterIconContainer } from "./styled";

export default function Footer() {
	const theme: ThemeContextType = useContext(ThemeContext);
	return (
		<FooterContainer theme={theme}>
			<FooterIconContainer>
				<IconLinks iconLinks={iconLinks} />
			</FooterIconContainer>
			<Anchor
				target="_blank"
				theme={theme}
				href="https://www.gnu.org/licenses/gpl-3.0.html"
			>
				GPLv3
			</Anchor>
			<p> © {new Date().getFullYear()} Weston Bailey</p>
		</FooterContainer>
	);
}
