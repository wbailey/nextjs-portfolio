import styled from "styled-components";
import { units, layout, fontSizes } from "@/components/common/styled/css";
import { ThemeProps } from "@/types";

export const LandingCodeContainer = styled.div`
	${layout.col};
`;

export const LandingCodeSnip = styled.code`
	padding: .075rem;
	font-family: Menlo, Monaco, Lucida Console, Liberation Mono,
		DejaVu Sans Mono, Bitstream Vera Sans Mono, Courier New, monospace;
	color: ${(props: ThemeProps) => props.theme.colors.secondary};
	${fontSizes.xSmall}
}
`;

export const LandingIconContainer = styled.div`
    ${layout.rowStatic}
    & > p {
        color: ${(props: ThemeProps) => props.theme.colors.tertiary};
        ${fontSizes.med}
        padding: 0 .5rem;
    }
    & div {
        ${layout.rowStatic}
        justify-content: center;
        align-items: center;
    }
}
`;

export const LandingSection = styled.section`
	padding: calc(${units.font.small} * 3) 0;
	min-height: 100vh;
	background-color: ${(props: ThemeProps) => props.theme.colors.primary};
`;

export const LandingTitle = styled.h1`
	${fontSizes.large}
	line-height: 1.2;
	text-align: left;
	padding: 0;
	margin: 0;
	color: ${(props: ThemeProps) => props.theme.colors.tertiary};
`;
