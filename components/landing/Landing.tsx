import { ScrollRefType, ThemeContextType } from "@/types";
import { useId } from "@/hooks";
import { useContext } from "react";
import { ThemeContext } from "@/state";
import { IconLinks } from "@/components/common";
import { iconLinks, codes } from "@/copy";
import {
	LandingCodeSnip,
	LandingSection,
	LandingTitle,
	LandingIconContainer,
	LandingCodeContainer,
} from "./styled";

interface Props {
	scrollRef: ScrollRefType;
}

export default function Landing({ scrollRef }: Props) {
	const theme: ThemeContextType = useContext(ThemeContext);

	const componentId: string = useId("Landing.jsx");

	const codeComponents = codes.map((code, idx) => (
		<LandingCodeSnip
			theme={theme}
			key={`landing-code-${idx}${componentId}`}
		>
			{code.replace(/ /g, "\u00a0")}
		</LandingCodeSnip>
	));

	return (
		<div ref={scrollRef}>
			<LandingSection theme={theme}>
				<div>
					<LandingTitle theme={theme}>Weston</LandingTitle>

					<LandingTitle theme={theme}>Bailey = {"{"}</LandingTitle>
				</div>

				<LandingCodeContainer>{codeComponents}</LandingCodeContainer>

				<LandingIconContainer theme={theme}>
					<p>lets be friends</p>

					<div>
						<IconLinks iconLinks={iconLinks} />
					</div>
				</LandingIconContainer>
			</LandingSection>
		</div>
	);
}
