import styled from "styled-components";
import { fontSizes, units, layout } from "@/components/common/styled/css";
import { ThemeProps } from "@/types";

export const AboutHeadShot = styled.img`
	padding: 1vh 1vw;
	border-radius: 50%;
	min-width: 400px;
	min-height: auto;
	max-width: 600px;
	max-haight: 600px;
	@media screen and (max-width: ${units.breakpoint.tablet}) {
		min-width: 300px;
		min-height: auto;
		max-width: 300px;
		max-height: 300px;
	}
}
`;

export const AboutSection = styled.section`
	padding: 2vw 0;
	min-height: 50vh;
	${layout.row}
	/* not needed? */
	justify-content: space-between;
	justify-content: center;
	align-items: center;
`;

export const AboutText = styled.div`
	${layout.row}
	justify-content: center;
	align-items: center;
	& div {
		${layout.col}
		max-width: 50vw;
		@media screen and (max-width: ${units.breakpoint.tablet}) {
			max-width: 90vw;
		}
	}
	& p {
		${fontSizes.small}
	}
`;

export const WrapperDiv = styled.div`
	color: ${(props: ThemeProps) => props.theme.colors.primary};
	background-color: ${(props: ThemeProps) => props.theme.colors.tertiary};
	& h2 {
		${fontSizes.med}
		padding-left: 2vw;
		padding-top: 10vh;
		margin: 0;
		@media screen and (max-width: ${units.breakpoint.tablet}) {
			padding-top: 5vh;
		}
	}
`;
