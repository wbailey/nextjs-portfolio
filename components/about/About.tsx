import { ThemeContext } from "../../state";
import { useContext } from "react";
import { Anchor } from "@/components/common/styled";
import { ScrollRefType, ThemeContextType } from "@/types";
import { AboutHeadShot, AboutSection, AboutText, WrapperDiv } from "./styled";

interface Props {
	scrollRef: ScrollRefType;
}

export default function About({ scrollRef }: Props) {
	const theme: ThemeContextType = useContext(ThemeContext);

	return (
		<div ref={scrollRef}>
			<WrapperDiv theme={theme}>
				<h2>About Me</h2>
				<AboutSection theme={theme}>
					<AboutText>
						<div>
							<p>
								I am a naturally inquisitive, creative software
								engineer that is driven by a passion to
								contribute elegant solutions to complex and
								asymmetric problems. I approach all problems by
								leveraging my drive to excel, my innate
								curiosity, and my experience as both a
								collaborator and leader.
							</p>

							<p>
								Currently I am a{" "}
								<strong> SEI Intrustor Lead</strong> @{" "}
								<Anchor
									href="https://generalassemb.ly/instructors/weston-bailey/26125"
									target="_blank"
									theme={theme}
								>
									General Assembly
								</Anchor>
								, and a{" "}
								<strong>curriculum writer/maintainer</strong> @{" "}
								<Anchor
									href="https://github.com/WDI-SEA"
									target="_blank"
									theme={theme}
								>
									WDI-SEA
								</Anchor>
								. I have previously been a web engineer and
								music software developer.
							</p>
						</div>
					</AboutText>

					<AboutHeadShot
						alt="Me, Weston!"
						src="/headshots/headshot-1.jpg"
					/>
				</AboutSection>
			</WrapperDiv>
		</div>
	);
}
