import {
	ThemeType,
	ThemeContextType,
	SvgType,
	ColorsType,
	ThemeActionsType,
	RegisteredThemeType,
} from "../types";
import { hexToRgb, Color, Solver } from "./_colors.js";

class Themes implements ThemeType {
	private static registered: RegisteredThemeType = {};
	name: string;
	colors: ColorsType;
	svg: SvgType;
	constructor(
		name: string,
		primary: string,
		secondary: string,
		tertiary: string,
		active: string = "",
		inActive: string = ""
	) {
		this.name = name;
		this.colors = {
			primary,
			secondary,
			tertiary,
		};
		this.svg = {
			active: active,
			inActive: inActive,
		};
	}
	static checkThemeIsRegistered(name: string): boolean {
		return Themes.registered[name] ? true : false;
	}
	static getTheme(name: ThemeActionsType): ThemeContextType {
		if (Themes.registered[name]) {
			return Themes.registered[name];
		} else {
			throw new Error(
				`Theme: ${name} not currently registered with as a valid theme.`
			);
		}
		return null;
	}

	static hexToSvg(hexCode: string): string {
		// TODO: update types here when refactoring _colors.js to .ts
		const rgb = hexToRgb(hexCode);
		if (!(rgb instanceof Array)) {
			throw new Error(
				`rgb "${rgb}" is not an instance of an array! Perhaps supplied parameter of "${hexCode}" is not a valid css hexcode?`
			);
		}
		const newColor = new Color(rgb[0], rgb[1], rgb[2]);
		const solver = new Solver(newColor);
		const result = solver.solve();
		return result.filter;
	}
	// calculating and adding to array of themes is not done at time of constructiong to avoid hydration errors, registration shoud be done in a useEffect
	calculateSvgColors(): ThemeType {
		this.svg.active = Themes.hexToSvg(this.colors.tertiary);
		this.svg.inActive = Themes.hexToSvg(this.colors.secondary);
		return this;
	}

	register(): ThemeType {
		Themes.registered[this.name] = this;
		return this;
	}

	unregister(): ThemeType {
		if (Themes.registered[this.name]) {
			delete Themes.registered[this.name];
		}
		return this;
	}
}

export default Themes;
