import { Dispatch, MutableRefObject } from "react";

// Theme Types
export interface ColorsType {
	primary: string;
	secondary: string;
	tertiary: string;
}

export interface SvgType {
	active: string;
	inActive: string;
}

export interface ThemeType {
	name: string;
	colors: ColorsType;
	svg: SvgType;
	calculateSvgColors: () => ThemeType;
	register: () => ThemeType;
	unregister: () => ThemeType;
	dispatchTheme?: Dispatch<ThemeActionsType>;
}

export interface ThemeProps {
	theme: ThemeType;
}

export interface RegisteredThemeType {
	[key: string]: ThemeType;
}
// TODO: ThemeContextType should be ThemeTupe | null, but is crashing deploy
export type ThemeContextType = any | null;

export type ThemeActionsType =
	| "dark"
	| "light"
	| "geocities"
	| "blank"
	| "custom";

export interface ColorStateType {
	primary: string;
	secondary: string;
	tertiary: string;
}

export type ColorNameType = "primary" | "secondary" | "tertiary";

export interface ColorActionType {
	name: ColorNameType;
	color: string;
}

// Navigation Types
export type NavItemType = "contact" | "about" | "techs" | "projects" | "";

export type ScrollRefType =
	| MutableRefObject<HTMLDivElement>
	| MutableRefObject<null>;

export interface ScrollRefsType {
	[key: string]: ScrollRefType;
}

// copy/data types
export interface DataObjectType {
	[key: string]: string;
}

export interface ProjectObjectType {
	link: string;
	title: string;
	img: {
		src: string;
		alt: string;
	};
	description: string;
}

export type DataStringArray = Array<string>;

export type DataObjectArray = Array<DataObjectType>;
