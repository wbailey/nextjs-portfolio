# Portfolio

this is my portfolio, the deployment can be viewed [here](https://www.weston-bailey.codes)

## Setting this project up

-   clone this repo
-   cd into the folder
-   run `npm i` to get the required dependancies
-   run `npm run dev` to build a development port of this app
-   navigate to `localhost:3000` in your web browser to view the app

## Sources

-   the amazing svg color algorithm (`./utils/_color.js`) is adapted from [this codepen](https://codepen.io/sosuke/pen/Pjoqqp) and [this stack overflow](https://stackoverflow.com/questions/42966641/how-to-transform-black-into-any-given-color-using-only-css-filters/43960991#43960991) and [this stackoverflow](https://www.stackoverflow.com/questions/22252472/how-to-change-the-color-of-an-svg-element)
-   the svg images themselves come from [this svg icon pack](https://www.svgrepo.com/collection/tech-brand-logos/1)
-   the version of `“@next/third-parties”` is `14.0.5-canary.38` due to this [issue](https://github.com/vercel/next.js/issues/57643)

## Todos

-   [x] refactor away from Sass to styled components
-   [x] add ability to choose theme
-   ~~[ ] theme select needs to be styled~~
-   [x] theme select should not disappear when viewed in mobile
-   [x] one theme has to be choosen as default, due to next's SSR, making a weird UX when first landing on the site if theme swaps real fast
    -   [x] posssible fix: have default all white theme, giving the imopression of a moment or two of page load while theme changes
-   [x] styled components are a little messy, should refactor dir/file structure
    -   idea: each section gets a dir, containing dependants. Create a common dir for reused componnents and css
-   [x] add custom theme mode
-   [ ] add geocities theme mode
-   [ ] add link to portfolio source in footer
-   [ ] add icons to project cards to replace links
-   [x] consolidate theme functionality down to one class: it registers new themes in a static array and returns the array in a static method "getValidThemes". Themes are created and precomputed with svg and a hook "useThemes" could return the Parent class. a method "getTheme" could check a given theme name against registered, precomputed themes and return a theme if tha name is valid, otherwise return a default theme. A method "setTheme" could be used to set the values of a certain theme before it is then dispatch (this is how custom theming could work).
-   [x] remove copyright and add creative commons liscence logo in footer https://creativecommons.org/about/downloads/
