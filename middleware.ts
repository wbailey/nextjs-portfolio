import { NextRequest, NextResponse } from "next/server";

export default function requestLogger(req: NextRequest) {
	if (req.nextUrl.pathname === "/") {
		console.log("ip:", req.ip);
		console.log("geo:", req.geo);
	}
	return NextResponse.next();
}
