import { GoogleAnalytics, GoogleTagManager } from "@next/third-parties/google";
import { useReducer, useEffect, useState, useRef } from "react";
import "@/styles/globals.css";
import { ThemeContext, themeReducer } from "@/state";
import { Themes } from "@/utils";
import { ThemeType } from "@/types";
import { AppProps } from "next/app";
import Script from "next/script";

const defaultTheme: ThemeType = new Themes(
	"blank",
	"#FFFFFF",
	"#FFFFFF",
	"#FFFFFF",
	"filter: invert(100%) sepia(94%) saturate(0%) hue-rotate(200deg) brightness(105%) contrast(107%);",
	"filter: invert(100%) sepia(0%) saturate(7500%) hue-rotate(119deg) brightness(121%) contrast(118%);"
);

defaultTheme.register();

function MyApp({ Component, pageProps }: AppProps) {
	const [theme, dispatchTheme] = useReducer(themeReducer, defaultTheme);
	const [forceReRender, setForceReRender] = useState<number>(0);
	const forceRenderScheduleRef = useRef<boolean>(false);

	useEffect(() => {
		const dark: ThemeType = new Themes(
			"dark",
			"#000000",
			"#FF69B4",
			"#00CED1"
		);
		dark.calculateSvgColors().register();
		const light: ThemeType = new Themes(
			"light",
			"#FFFFFF",
			"#0000FF",
			"#008000"
		);
		light.calculateSvgColors().register();
		const geoCities: ThemeType = new Themes(
			"geocitites",
			"#FFFFFF",
			"#0000FF",
			"#008000"
		);
		geoCities.calculateSvgColors().register();
		const prefersDarkMode: boolean = window.matchMedia(
			"(prefers-color-scheme:dark)"
		).matches;
		if (!prefersDarkMode) {
			dispatchTheme("light");
		} else {
			dispatchTheme("dark");
		}
	}, []);

	// janky workarounds ftw
	const forceRender = (callback?: Function) => {
		// only allow forced rendered if antoher is not already queued
		if (!forceRenderScheduleRef.current) {
			forceRenderScheduleRef.current = true;
			setTimeout(() => {
				// callback gets invoked first incase it has an impact on the rendering
				if (callback) {
					callback();
				}
				forceRenderScheduleRef.current = false;
				setForceReRender(forceReRender + (1 % Number.MAX_SAFE_INTEGER));
			}, 25); // 25 is a nice compromise between performance and noticable delay
		}
	};

	return (
		<ThemeContext.Provider value={{ ...theme, dispatchTheme, forceRender }}>
			<Script
				strategy="afterInteractive"
				src="https://www.googletagmanager.com/gtag/js?id=G-QT8TTDT6BY"
			/>
			<Script
				id="google-analytics"
				strategy="afterInteractive"
				dangerouslySetInnerHTML={{
					__html: `
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'G-QT8TTDT6BY', {
            page_path: window.location.pathname,
          });
        `,
				}}
			/>
			<Component {...pageProps} />
		</ThemeContext.Provider>
	);
}

// Only uncomment this method if you have blocking data requirements for
// every single page in your application. This disables the ability to
// perform automatic static optimization, causing every page in your app to
// be server-side rendered.
//
// MyApp.getInitialProps = async (appContext) => {
//   // calls page's `getInitialProps` and fills `appProps.pageProps`
//   const appProps = await App.getInitialProps(appContext);
//
//   return { ...appProps }
// }

export default MyApp;
