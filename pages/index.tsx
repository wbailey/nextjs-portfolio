import React, { useRef, useContext } from "react";
import Head from "next/head";
import { Navbar, Footer } from "@/components/common";
import { About, Projects, Techs, Landing } from "@/components";
import { ScrollRefsType, ThemeContextType } from "@/types";

export default function Index() {
	const scrollRefs: ScrollRefsType = {
		contact: useRef(null),
		about: useRef(null),
		projects: useRef(null),
		techs: useRef(null),
	};

	return (
		<div>
			<Head>
				<title>Weston Bailey</title>
				<link rel="icon" href="/favicon.svg" />
			</Head>

			<nav>
				<Navbar scrollRefs={scrollRefs} />
			</nav>

			<main>
				<Landing scrollRef={scrollRefs.contact} />

				<About scrollRef={scrollRefs.about} />

				<Techs scrollRef={scrollRefs.techs} />

				<Projects scrollRef={scrollRefs.projects} />
			</main>

			<footer>
				<Footer />
			</footer>
		</div>
	);
}
