export { default as about } from "./about";
export { default as iconLinks } from "./iconLinks";
export { default as codes } from "./codes";
export { default as projects } from "./projects";
export { default as smallProjects } from "./smallProjects";
export { default as techs } from "./techs";
