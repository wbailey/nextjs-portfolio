import { DataObjectArray } from "@/types";

const smallProjects: DataObjectArray = [
	{
		title: "XOR encryption:",
		link: "https://github.com/weston-bailey/xor-encryption",
		text: "a command line utility that can be used to encrypt files using an XOR encryption algorithm.",
	},
	{
		title: "Lexer:",
		link: "https://github.com/weston-bailey/python-lexer-calculator",
		text: "a tokenizer/lexer that can evaluate math expressions on the command line.",
	},
	{
		title: "Student Randomizer,",
		link: "https://github.com/weston-bailey/student-randomizer",
		text: "a command line tool that automates calling on students in class.",
	},
	{
		title: "RESTful API Planning Lesson,",
		link: "https://github.com/WDI-SEA/RESTful-API-planning",
		text: "a lesson to practice RESTful routing.",
	},
	{
		title: "React Cloudinary/Multipart Forms Lesson.",
		link: "https://github.com/weston-bailey/react-cloudinary-multipart-form",
		text: "A image upload client and server using Express, React, and cloudinary, written for a lesson",
	},
	{
		title: "Auth Research Lab,",
		link: "https://github.com/WDI-SEA/auth-research-lab",
		text: "a research based lesson to study user auth fundamentals",
	},
	{
		title: "Homework cloner",
		link: "https://github.com/weston-bailey/homework-cloner",
		text: "a team project that automates homework evaluation",
	},
	{
		title: "Intro to MERN auth:",
		link: "https://github.com/WDI-SEA/MERN-auth-codealongs",
		text: "code along lessons to learn about fundamental user auth concepts",
	},
];

export default smallProjects;
