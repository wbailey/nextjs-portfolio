import { DataObjectArray } from "@/types";

const iconLinks: Array<{
	id: string;
	src: string;
	alt: string;
	href: string;
}> = [
	{
		id: "gitlab",
		src: "/icons/gitlab.svg",
		alt: "the gitlab logo",
		href: "https://gitlab.com/wbailey",
	},
	{
		id: "github",
		src: "/icons/github.svg",
		alt: "the github logo",
		href: "https://github.com/weston-bailey",
	},
	{
		id: "linkedin",
		src: "/icons/linkedin.svg",
		alt: "linked logo",
		href: "https://www.linkedin.com/in/weston-bailey-545b591ab/",
	},
	// {
	//   src: '/icons/discord.svg',
	//   alt: 'an image of the discord logo',
	//   href: 'https://discord.gg/user/name#returnvoid#9673'
	// },
	{
		id: "email",
		src: "/icons/gmail.svg",
		alt: "email logo",
		href: "mailto:weston.bailey@protonmail.com",
	},
	// {
	// 	id: "resume",
	// 	src: "/icons/doc.svg",
	// 	alt: "a document",
	// // note, this is now a private doc
	// 	href: "https://docs.google.com/document/d/1_2POFzKBGKJCKoXuVQJU21CuamnUDUHW/edit?usp=sharing&ouid=105018614871864947731&rtpof=true&sd=true",
	// },
	// {
	//   id: 'zoom',
	//   src: '/icons/zoom.svg',
	//   alt: 'the zoom logo',
	//   href: 'https://generalassembly.zoom.us/j/2982793978?pwd=ek4vZFBJOHZscEk1d2ZaZGhPSFVhZz09'
	// },
	// {
	//   id: 'soundcloud',
	//   src: '/icons/soundcloud.svg',
	//   alt: 'the soundcloud logo',
	//   href: 'https://soundcloud.com/return-void-809171048'
	// },
];

export default iconLinks;
