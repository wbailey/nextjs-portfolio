import { DataStringArray } from "@/types";

const codes: DataStringArray = [
	"    software engineer,",
	"    educator,",
	"    collaborator,",
	"    musician,",
	"    nerd",
	"}",
];

export default codes;
