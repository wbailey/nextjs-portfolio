import { ProjectObjectType } from "@/types";

const projects: Array<ProjectObjectType> = [
	{
		link: "https://gitlab.com/wbailey/emu0x08",
		title: "emu0x08",
		img: {
			src: "/img/emu0x08.png",
			alt: "emu0x08 running the classic IBM logo chip 8 rom.",
		},
		description:
			"A CHIP-8 emulator for *nix systems written in c that uses SDL2, Doxygen and Criterion.",
	},
	{
		link: "https://github.com/weston-bailey/js-physics",
		title: "JS Physics",
		img: {
			src: "/img/physics.gif",
			alt: "A Gravity attractor physcis demo",
		},
		description:
			"A rigid body physics simulation engine written in Javascript that uses HTML5 canvas for rendering.",
	},
	{
		link: "https://github.com/weston-bailey/tensorflow-text-gen",
		title: "AI Dictionary",
		img: {
			src: "/img/dictionary.png",
			alt: "The AI Dictionary command prompt",
		},
		description:
			"The AI Dictionary uses its nueral nets to come up with new definitions to words, even words that do not exist yet. Also comes in an AI Lovecraftian horror generator flavor as well.",
	},
	{
		link: "https://github.com/weston-bailey/machine-learning-playground",
		title: "Machine Learning Playground",
		img: {
			src: "/img/ml-playground-2.gif",
			alt: "Machine Learning Playground homepage.",
		},
		description:
			"Using Tensorflow.js, the machine learning playground aims to let users understand core AI and ML concepts by allowing them to interact directly with neural nets that they have created and trained in the browser.",
	},
	{
		link: "https://github.com/weston-bailey/hack-a-thon",
		title: "Lunar Tribune",
		img: {
			src: "/img/lunar-1.png",
			alt: "The Lunar Tribune Playground homepage.",
		},
		description:
			"The Lunar Tribune is a satirical newsite with a kitchy, pulp-scifi retro-futuristic theme. Created in a 24hr hack-a-thon working with a team of UX designers, the project serves a a proof-of-concept with mock functionality to display the vision created by the UX team.",
	},
	{
		link: "https://github.com/weston-bailey/innervue",
		title: "innervue",
		img: {
			src: "/img/innervue-1.png",
			alt: "innervue homepage",
		},
		description:
			"Giving job applicants the key tools to ace the interview. Users are given mock interview question prompts, and are able to practice speaking their response to receive intuitive feedback on how to improve.",
	},
	{
		link: "https://github.com/weston-bailey/Quakebook",
		title: "Quakebook",
		img: {
			src: "/img/quakebook-homepage.png",
			alt: "The Quakebook homepage.",
		},
		description:
			"The world's #1 social media platform with a community centered around seismic activity data. Quakebook's server collects data from the usgs earthquake API as seismic activity occurs, gives users access to the data as they engage with the Quakebook Community.",
	},
	{
		link: "https://github.com/weston-bailey/Hyperdrive",
		title: "Hyperdrive",
		img: {
			src: "./img/hyperdrive-1.gif",
			alt: "Hyperdive Gameplay.",
		},
		description:
			"A retro-futuristic, space-themed, vertically scrolling infinite runner game that was aesthetically modeled on golden era arcade games such as Zaxxon, Gradius and Asteroids.",
	},
	{
		link: "https://github.com/weston-bailey/m4l-plugins",
		title: "Max 4 Live plugins",
		img: {
			src: "./img/m4l-plugins.png",
			alt: "Max 4 Live Plugins",
		},
		description:
			"A collection of Sound Design and MIDI plugins for Ableton Live",
	},
];

export default projects;
