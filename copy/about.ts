import { DataObjectType } from "@/types";

const about: DataObjectType = {
	statement:
		"I am a naturally inquisitive, creative software engineer that is driven by a passion to contribute elegant solutions to complex and asymmetric problems. I approach all problems by leveraging my drive to excel, my innate curiosity, and my experience as both a collaborator and leader.",
	previous:
		"I have previously been a web engineer and music software developer.",
};

export default about;
