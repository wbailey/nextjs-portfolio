import { v1 as uuid } from "uuid";
import { useRef } from "react";

export default (componentName: string): string =>
	componentName + "-" + useRef(uuid()).current;
